package ru.fdoke.raic3dviewer.model;

import java.awt.*;
import java.io.Serializable;

public class DDrawLine implements Serializable {
    private static final long serialVersionUID = -9123489471259841258L;

    public DVector3f from;
    public DVector3f to;
    public Color color;

    /**
     * Simple from to line.
     */
    public DDrawLine(DVector3f from, DVector3f to, Color color) {
        this.from = from;
        this.to = to;
        this.color = color;
    }

    /**
     * Line by direction.
     * Dir must be normalized.
     */
    public DDrawLine(DVector3f from, DVector3f dir, float size, Color color) {
        this.from = from;
        this.to = new DVector3f(from.x + dir.x * size, from.y + dir.y * size, from.z + dir.z * size);
        this.color = color;
    }
}
