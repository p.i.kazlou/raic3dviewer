package ru.fdoke.raic3dviewer.control;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;

public class ControlForm extends JFrame {
    public JLabel tickLabel;
    public JSlider tickSlider;
    public JSlider tpsSlider;
    public JButton prev;
    public JButton next;
    public JButton playPause;
    public JButton realTime;

    public volatile boolean kostil = false;

    public ControlForm() {
        setTitle("Controls");
    }

    public void init(MouseInputAdapter onPrevButton,
                     MouseInputAdapter onNextButton,
                     MouseInputAdapter onPlayPauseButton,
                     MouseInputAdapter onRealTimeButton,
                     ChangeListener onTickSliderChange,
                     ChangeListener onTpsSliderChange,
                     Runnable stateUpdater) {
        setResizable(false);
        setSize(600, 80);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setLocationByPlatform(true);

        JPanel mainControlPanel = new JPanel();
        mainControlPanel.setLayout(new BoxLayout(mainControlPanel,BoxLayout.LINE_AXIS));
        prev = new JButton("Prev frame");
        prev.addMouseListener(onPrevButton);
        next = new JButton("Next frame");
        next.addMouseListener(onNextButton);
        playPause = new JButton("Pause");
        playPause.addMouseListener(onPlayPauseButton);
        realTime = new JButton("Real time");
        realTime.addMouseListener(onRealTimeButton);
        tpsSlider = new JSlider(1, 150);
        tpsSlider.createStandardLabels(1);
        tpsSlider.setValue(100);
        tpsSlider.setSize(100, 20);
        tpsSlider.addChangeListener(onTpsSliderChange);
        tpsSlider.setPaintTicks(true);
        tpsSlider.setSnapToTicks(true);
        tpsSlider.setMajorTickSpacing(25);
        tpsSlider.setMinorTickSpacing(1);
        tpsSlider.setToolTipText("100");

        mainControlPanel.add(prev, BorderLayout.WEST);
        mainControlPanel.add(next, BorderLayout.CENTER);
        mainControlPanel.add(playPause, BorderLayout.EAST);
        mainControlPanel.add(realTime, BorderLayout.EAST);
        mainControlPanel.add(tpsSlider, BorderLayout.EAST);

        JPanel tickControlPanel = new JPanel();
        tickControlPanel.setLayout(new BoxLayout(tickControlPanel, BoxLayout.X_AXIS));
        tickSlider = new JSlider(0, 0);
        tickSlider.addChangeListener(onTickSliderChange);
        tickControlPanel.add(tickSlider);
        tickLabel = new JLabel("000000/000000");
        tickControlPanel.add(tickLabel);

        add(mainControlPanel, BorderLayout.NORTH);
        add(tickControlPanel, BorderLayout.SOUTH);
        setVisible(true);

        Thread stateUpdaterThread = new Thread(stateUpdater);
        stateUpdaterThread.setDaemon(true);
        stateUpdaterThread.start();
    }
}
