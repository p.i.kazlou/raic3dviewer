# CodeBall 2018 RAIC viewer

This project is adaptation of FDoke's Raic3dViewer for CodeBall competition.

Original Raic3dViewer: https://gitlab.com/FDoKE/raic3dviewer

CodeBall competition: http://2018.russianaicup.ru/

Java 8 is required.

### Controls
- Left/Right - on main window - backward/forward playing
- Up Arrow - return to realtime
- Down Arrow - pause/play
- Swing panel to control

### How to use
1) Run ServerStart.java
2) Copy all model classes to your project
3) Make socket connection to a server 
4) Make pool of draw commands or send them directly
5) Done

##### Example
Run ServerStart.java and then TestClient.java.
Feel free to close/hotswap/rerun TestClient.java without stopping server.

### Protocol
All ticks must start with DStart, and end with DEnd.
- DDrawArrow - aligned arrow with from/to position
- DDrawLine - simple line
- DDrawBox - box without rotation with position/size or from/to creating
- DDrawSphere - simple sphere with position/radius
